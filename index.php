<?php session_start(); ?>
<!DOCTYPLE>
<?php
include ("functions/functions.php");
?>
<html>
    <head>
        <!-- title ime se pokazuje u broseru u novom tabu -->
        <title>Road Runner</title>
        <!-- povezivanje sa bootstrapom -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- povezivanje sa css fajlom koji se nalzi u css folderu pod imenom style.css -->
        <link rel="stylesheet" type="text/css" href="css/style.css">

        <!-- povezivanje sa jqerujem -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
        <!-- povezivanje sa javaskriptom u folderu js pod imenom js.js -->
        <script src="js/js.js"></script> 
    </head>
    <body>
        <!-- pocetak navigacionog bara -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- RoadRuner ima css stil da se promeni boja u narandzastu i link na glavnoj strani index.php
                    omugucu je se kad korisnik klikne da ga vrati na pocetnu stranu-->
                    <a class="navbar-brand" href="index.php" style="color: orange;">RoadRunner</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <!-- neodredjena lista(postoji i odredjena kad ispred pise 1,2,3) sa elemtima koji vode ka
                    svim proizvodima, onama starnicim i  lokaciji-->
                    <ul class="nav navbar-nav">
                        <li><a href="sviproizvodi.php">Proizvodi</a></li>
                        <li><a href="onama.php">O nama<span class="sr-only">(current)</span></a></li>
                        <li><a href="lokacija.php">Lokacija</a></li>

                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <!-- kada u polju type pise text to znaci da je to polje za unu nekog texta, ovde bi trebao
                            korisnik da unec ime zeljenog proizvoda i da izbaci ako proizvod postji ima css stil
                            margin-right da bi se odvoji od desne strane 5 pixela -->
                            <input type="text" class="form-control" placeholder="Search" style="margin-right: 5px;">
                        </div>
                        <!-- tugme za pretranu uzima text iz predhodnog dela, ima css stil
                        jer dugme nije stojalo u ravni pa da ga vratimo :) -->
                        <button type="submit" class="btn btn-default" style="margin: 0 auto;">Potvrdi</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <!-- vodi ka korpi ima ikonicu korpe koja se poziva pucem kalse(class) glyphicon-shopping-cart
                        deo koji se nali izmenju <? ?> je php deo i pokacuje koliko razlicitih proizvoda postoji u korpi-->
                        <li><a href="cart.php" class="glyphicon glyphicon-shopping-cart"> <?php total_items(); ?> Proizvoda</a></li>

                        <!-- pocinje lista koja ila pit padjuce liste -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Korisnik <span class="caret"></span></a>
                            <ul class="dropdown-menu">

                                <!-- ako je korisnik ulogova lista ce pokazivati moj profil i odjavi se -->
                                <?php if (isset($_SESSION['user'])) { ?>
                                    <li><a href="profile.php">Moj profil</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="logout.php" style="color: orange;">Odjavi se</a></li>
                                <?php } else { ?>
                                    <!-- ako korasnik nije ulogova lista ce pokazivati prijavljivanje i registraciju sa linkovima 
                                    koji vode ka tim stranicima adtribut href pa adresa vodi kad tom linku-->
                                    <li><a href="login.php">Prijavljivanje</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="register.php" style="color: orange;">Registracija</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>  

        <!-- slajd shou koji poziva slike koje ce biti pirkazene ka pocetnoj strani 
        dodeli sli smo id elemt koji je definisam u css fajlu-->
        <div id="slideshow">
            <div>
                <img src="images/1.jpg" id="slika"> <!-- img scr je atribut koji poziva sliku-->
            </div>
            <div>
                <img src="images/2.jpg" id="slika">
            </div>
            <div>
                <img src="images/3.jpg" id="slika">
            </div>
            <div>
                <img src="images/4.jpg" id="slika">
            </div>
        </div>

        <h2>Nasi Proizvodi:</h2> <!-- ispisu tekst nasi proizvodi postoje razlicite velicine h1 h2 h3 h4 h5, h1 najveci -->
        <br>
        <div>
            <?php
            vrste();
            ?>
        </div>

        <br>
        <!-- pocetak futer sa ne oderedjenom listom
        odrednjena lista ima tag ol a ne oderdjena ima ul-->
        <footer>
            <!-- div elementu smo dodelili calsu footer koju smo definislai u css fajlu -->
            <div class="footer">
                <nav>
                    <ul>
                        <!-- li su elemetni ne odredjene liste -->
                        <li><a href="index.php">HOME</a></li>
                        <li><a href="sviproizvodi.php">PROIZVODI</a></li>
                        <li><a href="onama.php">O NAMA</a></li>
                        <li><a href="lokacija.php">KAKO NAS NACI</a></li>
                        <li><a href="zaposleni.php">ZAPOSLENI</a></li>
                    </ul>
                </nav>
            </div>
        </footer>
    </body>
</html>